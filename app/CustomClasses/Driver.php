<?php
namespace App\CustomClasses;

class Driver
{
	public $icon;
	public $nombre;
	public $lat;
	public $lon;

    public function __construct($lat, $lon){
    	$this->icon = 'assets/images/car.png';
        $this->nombre = $this->setName();
        $this->lat = $lat;
        $this->lon = $lon;
    }

    private function setName(){
    	$names = array(
	       'Divani',
	       'Josue',
	       'Andrea',
	       'Aurelio',
	       'Zoey',
	       'Sarah',
	       'Genesis',
	       'Estela',
           'Erick',
           'Maylo',
           'Ulises',
           'Carlos',
           'Adriana',
           'Adrian',
           'Nicole',
           'Bruno',
           'Alberto',
	    );
    	$random_name = $names[mt_rand(0, sizeof($names) - 1)];

        return $random_name;
    }
}