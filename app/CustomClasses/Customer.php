<?php
namespace App\CustomClasses;

class Customer
{
	public $icon;
	public $nombre;
	public $lat;
	public $lon;

    public function __construct($lat, $lon){
    	$this->icon = 'assets/images/customer.png';
    	$this->lat = $lat;
    	$this->lon = $lon;
    	$this->nombre = $this->setName();
    }

    private function setName(){
    	$names = array(
	       'Daniel',
	       'Juan',
	       'Andrea',
	       'Aurelio',
	       'Sarahi',
	       'Genesis',
	       'Arturo',
	       'Sandra',
	       'Leslie',
	       'Donovan',
	       'Alexis',
	       'Dorian',
	       'Diana',
	       'Marco',
	       'Guicel',
	       ''
	    );
    	$random_name = $names[mt_rand(0, sizeof($names) - 1)];

        return $random_name;
    }
}