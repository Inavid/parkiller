<?php
namespace App\CustomClasses;
use GuzzleHttp\Client;

class Geo
{
	private $apiKey;
	private $circleBoundSWLat;
	private $circleBoundSWLon;
	private $circleBoundNELat;
	private $circleBoundNELon;
	private $radius;
	private $circleCenterLat;
	private $circleCenterLon;

	public function __construct(){
		$this->apiKey = getenv('APIKEY_GMAPS');
		#De momento fijos 
		$this->circleBoundSWLat = 19.40966349431761;
		$this->circleBoundSWLon = -99.14772702598998;
		$this->circleBoundNELat = 19.445596105682387;
		$this->circleBoundNELon = -99.10962497400999;
		$this->circleCenterLat = 19.4276298;
		$this->circleCenterLon =  -99.128676;
		$this->radius = 2000;
	}

	/**
	* Metodo para obtener puntos de conductor aleatoriamente dentro de un area circular
	* @param Int $number_drivers --> Numero de conductores
	* @return Array $drivers --> Se regresa arreglo de objetos Driver con información requerida
	*/
	public function getDrivers($number_drivers){
		for ($i=0; $i < $number_drivers; $i++) { 
			$drivers_coordinates = $this->randomPointIntoArea();
			$drivers[$i] = new Driver($drivers_coordinates['lat'], $drivers_coordinates['lon']);
		}

		return $drivers;
	}

	/**
	* Metodo para obtener puntos de clientes aleatoriamente dentro de un area circular
	* @param Int $number_customer --> Numero de clientes
	* @return Array $customers --> Se regresa arreglo de objetos Customer con información requerida
	*/
	public function getCustomers($number_customers){
		for ($i=0; $i < $number_customers; $i++) { 
			$customers_coordinates = $this->randomPointIntoArea();
			$customers[$i] = new Customer($customers_coordinates['lat'], $customers_coordinates['lon']);
		}

		return $customers;
	}

	/**
	* Metodo para obtener el arreglo que asocia los conductores con sus clientes mas cercanos asi como la información de distancia y tiempo entre ellos
	* @param Array $drivers --> Arreglo de objetos Driver
	* @param Array $customers --> Arreglo de objetos Customer
	* @return Array $customers --> Se regresa arreglo de objetos Customer con información requerida
	*/
	public function getNearestRelations($drivers, $customers){
		foreach ($drivers as $key => $driver) {
			$destinations = "";
			foreach ($customers as $key2 => $customer) {
				$destinations .= trim($customer->lat).','.trim($customer->lon).'|';
			}

			$destinations = substr_replace($destinations, "|", -1);

			$result = $this->getDistanceBetweenDriverAndCustomers($driver->lat, $driver->lon, $destinations);

			$nearest_relations[] = array(
				'driver' 		=> $driver,
				'customer' 		=> $customers[$result['data']->index],
				'result_api' 	=> $result['data']
			);
			unset($customers[$result['data']->index]);
			$customers = array_values($customers);
		}

		foreach ($customers as $key => $customer) {
			$nearest_relations[] = array(
				'driver' 	 => null,
				'customer' 	 => $customer,
				'result_api' => null
			);
		}
		
		return $nearest_relations;
	}	

	/**
	* Metodo para obtener un punto aleatorio dentro de un area (circulo en este caso)
	* @return Array $point --> Se regresa arreglo de latitud y longitud del punto calculado geometricamente
	*/
	private function randomPointIntoArea(){
		$distance = $this->radius + 1;
		while ($distance > $this->radius) {
			$ptLat = (rand(0, 10) / 10) * ($this->circleBoundNELat - $this->circleBoundSWLat) + $this->circleBoundSWLat;
	        $ptLng = (rand(0, 10) / 10) * ($this->circleBoundNELon - $this->circleBoundSWLon) + $this->circleBoundSWLon;
	        $distance 		= $this->getDistanceBetweenPoints($ptLat, $ptLng, $this->circleCenterLat, $this->circleCenterLon);
		}

	    $point = array(
	    	'lat' => $ptLat,
	    	'lon' => $ptLng
	    );    
  
  		return $point;
	}

	/**
	* Se calcula geometricamente la distancia entre dos puntos dados
	* @param Double $lat1 --> Latitud del punto 1
	* @param Double $lon1 --> Longitud del punto 1
	* @param Double $lat2 --> Latitud del punto 2
	* @param Double $lon2 --> Longitud del punto 2
	* @return Double $result --> Se regresa distancia calculada geometricamente entre el punto 1 y el punto 2
	*/
	private function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2) {
    	$theta 	= $lon1 - $lon2;
    	$dist 	= sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    	$dist 	= acos($dist);
    	$dist 	= rad2deg($dist);
    	$miles 	= $dist * 60 * 1.1515;
    	$result = ($miles * 1.609344) * 1000;
    	return $result;
	}

	/**
	* Se calcula por medio de la API de google 
	* @param Double $lat1 --> Latitud del conductor (origen)
	* @param Double $lon1 --> Longitud del conductor (origen)
	* @param String $destinations --> Cadena preparada de puntos de destino (clientes)
	* @return Array $result --> Se regresa arreglo con resultado, mensaje e informacion de ruta mas cercana
	*/
	private function getDistanceBetweenDriverAndCustomers($latDriver, $lonDriver, $destinations) {
		$origin 	 = trim($latDriver) .','. trim($lonDriver);
    	$client = new Client(['base_uri' => 'http://maps.googleapis.com/']);

    	$request = $client->createRequest('GET', 'http://maps.googleapis.com/maps/api/distancematrix/json');
		$query = $request->getQuery();
		$query->set('origins', $origin);
		$query->set('destinations', $destinations);
		$query->set('mode', 'walking');
		$query->set('sensor', 'false');
		$query->set('language', 'es');
		$response = $client->send($request);
		if ($response->getStatusCode() == 200) {
			$adresses = json_decode($response->getBody());
			foreach ($adresses->rows[0]->elements as $key => $element) {
				$distance = $element->distance->value;
				if($element->distance->value <= $distance){
					$distance = $element->distance->value;
					$selected = $element;
					$selected->index = $key;
				}
			}	
			$result = array('success' => true, 'msg' => 'Ok', 'data' => $selected);
		} else {
			$result = array('success' => false, 'msg' => 'El servicio no respondio correctamente', 'data' => null);
		}

    	return $result;
	}

}






