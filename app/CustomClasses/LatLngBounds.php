<?php 
namespace App\CustomClasses;
use App\CustomClasses\SphericalGeometry;
use App\CustomClasses\LatLng;

class LatLngBounds
{
    protected $_LatBounds;
    protected $_LngBounds;
    
    /**
     * $LatLngSw South West LatLng object
     * $LatLngNe North East LatLng object
     */
    public function __construct($LatLngSw = null, $LatLngNe = null) 
    {   
        if ((!is_null($LatLngSw) && !($LatLngSw instanceof LatLng))
            || (!is_null($LatLngNe) && !($LatLngNe instanceof LatLng)))
        {
            trigger_error('LatLngBounds class -> Invalid LatLng object.', E_USER_ERROR);
        }
        
        if ($LatLngSw)
        {
            $LatLngNe = !$LatLngNe ? $LatLngSw : $LatLngNe;
            $sw = SphericalGeometry::clampLatitude($LatLngSw->getLat());
            $ne = SphericalGeometry::clampLatitude($LatLngNe->getLat());
            $this->_LatBounds = new LatBounds($sw, $ne);
            
            $sw = $LatLngSw->getLng();
            $ne = $LatLngNe->getLng();
            
            if (360 <= $ne - $sw)
            {
                $this->_LngBounds = new LngBounds(-180, 180);
            }
            else 
            {
                $sw = SphericalGeometry::wrapLongitude($sw);
                $ne = SphericalGeometry::wrapLongitude($ne);
                $this->_LngBounds = new LngBounds($sw, $ne);
            }
        } 
        else 
        {
            $this->_LatBounds = new LatBounds(1, -1);
            $this->_LngBounds = new LngBounds(180, -180);
        }
    }
    
    public function getLatBounds()
    {
        return $this->_LatBounds;
    }
    
    public function getLngBounds()
    {
        return $this->_LngBounds;
    }
    
    public function getCenter()
    {
        return new LatLng($this->_LatBounds->getMidpoint(), $this->_LngBounds->getMidpoint());
    }
    
    public function isEmpty()
    {
        return $this->_LatBounds->isEmpty() || $this->_LngBounds->isEmpty();
    }
    
    public function getSouthWest()
    {
        return new LatLng($this->_LatBounds->getSw(), $this->_LngBounds->getSw(), true);
    }
    
    public function getNorthEast()
    {
        return new LatLng($this->_LatBounds->getNe(), $this->_LngBounds->getNe(), true);
    }
    
    public function toSpan()
    {
        $lat = $this->_LatBounds->isEmpty() ? 0 : $this->_LatBounds->getNe() - $this->_LatBounds->getSw();
        $lng = $this->_LngBounds->isEmpty() 
            ? 0 
            : ($this->_LngBounds->getSw() > $this->_LngBounds->getNe() 
                ? 360 - ($this->_LngBounds->getSw() - $this->_LngBounds->getNe())
                : $this->_LngBounds->getNe() - $this->_LngBounds->getSw());
        
        return new LatLng($lat, $lng, true);
    }
    
    public function toString()
    {
        return '('. $this->getSouthWest()->toString() .', '. $this->getNorthEast()->toString() .')';
    }
    
    public function toUrlValue($precision = 6)
    {
        return $this->getSouthWest()->toUrlValue($precision) .','. 
            $this->getNorthEast()->toUrlValue($precision);
    }
    
    public function equals($LatLngBounds)
    {
        return !$LatLngBounds 
            ? false 
            : $this->_LatBounds->equals($LatLngBounds->getLatBounds()) 
                && $this->_LngBounds->equals($LatLngBounds->getLngBounds());
    }
    
    public function intersects($LatLngBounds)
    {
        return $this->_LatBounds->intersects($LatLngBounds->getLatBounds()) 
            && $this->_LngBounds->intersects($LatLngBounds->getLngBounds());
    }
    
    public function union($LatLngBounds)
    {
        $this->extend($LatLngBounds->getSouthWest());
        $this->extend($LatLngBounds->getNorthEast());
        return $this;
    }
    
    public function contains($LatLng)
    {
        return $this->_LatBounds->contains($LatLng->getLat()) 
            && $this->_LngBounds->contains($LatLng->getLng());
    }
    
    public function extend($LatLng)
    {
        $this->_LatBounds->extend($LatLng->getLat());
        $this->_LngBounds->extend($LatLng->getLng());
        return $this;    
    }
}


// DO NOT USE THE CLASSES BELOW DIRECTLY


class LatBounds
{
    protected $_swLat;
    protected $_neLat;
    
    public function __construct($swLat, $neLat) 
    {
        $this->_swLat = $swLat;
        $this->_neLat = $neLat;
    }
    
    public function getSw()
    {
        return $this->_swLat;
    }
    
    public function getNe()
    {
        return $this->_neLat;
    }
    
    public function getMidpoint()
    {
        return ($this->_swLat + $this->_neLat) / 2;
    }
    
    public function isEmpty()
    {
        return $this->_swLat > $this->_neLat;
    }
    
    public function intersects($LatBounds)
    {
        return $this->_swLat <= $LatBounds->getSw() 
            ? $LatBounds->getSw() <= $this->_neLat && $LatBounds->getSw() <= $LatBounds->getNe() 
            : $this->_swLat <= $LatBounds->getNe() && $this->_swLat <= $this->_neLat;
    }
    
    public function equals($LatBounds)
    {
        return $this->isEmpty() 
            ? $LatBounds->isEmpty() 
            : abs($LatBounds->getSw() - $this->_swLat) 
                + abs($this->_neLat - $LatBounds->getNe()) 
                <= SphericalGeometry::EQUALS_MARGIN_ERROR;
    }
    
    public function contains($lat)
    {
        return $lat >= $this->_swLat && $lat <= $this->_neLat;
    }
    
    public function extend($lat)
    {
        if ($this->isEmpty()) 
        {
            $this->_neLat = $this->_swLat = $lat;
        }
        else if ($lat < $this->_swLat) 
        { 
            $this->_swLat = $lat;
        }
        else if ($lat > $this->_neLat) 
        {
            $this->_neLat = $lat;
        }
    }
}



class LngBounds
{
    protected $_swLng;
    protected $_neLng;
    
    public function __construct($swLng, $neLng) 
    {
        $swLng = $swLng == -180 && $neLng != 180 ? 180 : $swLng;
        $neLng = $neLng == -180 && $swLng != 180 ? 180 : $neLng;
        
        $this->_swLng = $swLng;
        $this->_neLng = $neLng;
    }
    
    public function getSw()
    {
        return $this->_swLng;
    }
    
    public function getNe()
    {
        return $this->_neLng;
    }
    
    public function getMidpoint()
    {
        $midPoint = ($this->_swLng + $this->_neLng) / 2;
        
        if ($this->_swLng > $this->_neLng) 
        {
            $midPoint = SphericalGeometry::wrapLongitude($midPoint + 180);
        }
        
        return $midPoint;
    }
    
    public function isEmpty()
    {
        return $this->_swLng - $this->_neLng == 360;
    }
    
    public function intersects($LngBounds)
    {
        if ($this->isEmpty() || $LngBounds->isEmpty()) 
        {
            return false;
        }
        else if ($this->_swLng > $this->_neLng) 
        {
            return $LngBounds->getSw() > $LngBounds->getNe() 
                || $LngBounds->getSw() <= $this->_neLng 
                || $LngBounds->getNe() >= $this->_swLng;
        }
        else if ($LngBounds->getSw() > $LngBounds->getNe()) 
        {
            return $LngBounds->getSw() <= $this->_neLng || $LngBounds->getNe() >= $this->_swLng;
        }
        else 
        {
            return $LngBounds->getSw() <= $this->_neLng && $LngBounds->getNe() >= $this->_swLng;
        }
    }
    
    public function equals($LngBounds)
    {
        return $this->isEmpty() 
            ? $LngBounds->isEmpty() 
            : fmod(abs($LngBounds->getSw() - $this->_swLng), 360) 
                + fmod(abs($LngBounds->getNe() - $this->_neLng), 360) 
                <= SphericalGeometry::EQUALS_MARGIN_ERROR;   
    }
    
    public function contains($lng)
    {
        $lng = $lng == -180 ? 180 : $lng;
        
        return $this->_swLng > $this->_neLng 
            ? ($lng >= $this->_swLng || $lng <= $this->_neLng) && !$this->isEmpty()
            : $lng >= $this->_swLng && $lng <= $this->_neLng;
    }
    
    public function extend($lng)
    {
        if ($this->contains($lng)) 
        {
            return;
        }
        
        if ($this->isEmpty())
        {
            $this->_swLng = $this->_neLng = $lng;
        }
        else 
        {
            $swLng = $this->_swLng - $lng;
            $swLng = $swLng >= 0 ? $swLng : $this->_swLng + 180 - ($lng - 180);
            $neLng = $lng - $this->_neLng;
            $neLng = $neLng >= 0 ? $neLng : $lng + 180 - ($this->_neLng - 180);
            
            if ($swLng < $neLng) 
            {
                $this->_swLng = $lng;
            }
            else 
            {
                $this->_neLng = $lng;
            }
        }
    }
}