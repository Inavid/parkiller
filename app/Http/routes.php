<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::post('/map', 'IndexController@map');
Route::get('/getmarkers', 'IndexController@getmarkers');

/*
Rutas de API
*/
Route::get('/api/getInfo', 'ApiController@getmarkers');
