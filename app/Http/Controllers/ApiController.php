<?php
namespace App\CustomClasses;
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\CustomClasses\Geo;

class ApiController extends Controller
{
    #METOFO GET de API, devuelve el arreglo correspondiente de conductores, clientes y relaciones de distancia
    public function getmarkers(Request $request){
        $all = $request->all();
        $geo = new Geo();
        $drivers  = $geo->getDrivers($all['driver_number']);
        $customer = $geo->getCustomers($all['customer_number']);
        $coordinates_to_draw  = $geo->getNearestRelations($drivers, $customer);
        
        return response()->json($coordinates_to_draw);
    }

}

?>