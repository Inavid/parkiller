<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

class IndexController extends Controller
{
    #Render pagina inicial
    public function index(){
        return view('index.index');
    }

    #Render vista de mapa
    public function map(Request $request){
    	$all = $request->all();
    	
    	return view('map.map', ['customer_number' => $all['customer_number'], 'driver_number' => $all['driver_number']]);
    }
}

?>