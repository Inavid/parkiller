<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prueba Parkiller</title>
 	<!-- Styles -->
	{!! Html::style('assets/bootstrap/css/bootstrap.css') !!}
	{!! Html::style('assets/css/map.css') !!}
	<!-- {!! Html::style('assets/css/style.css') !!} -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-collapse collapse text-center">
				<h4>Prueba Parkiller</h4>
			</div>
		</div>
	</nav>
	@yield('content')
 
	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	{!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
	{!! Html::script('assets/js/map.js') !!}
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<?php echo getenv('APIKEY_GMAPS'); ?>&callback=initMap&sensor=false"></script>
	<script type ="text/javascript" src="http://www.geocodezip.com/scripts/v3_epoly.js"></script>
	<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
	</script>
	<script type="text/javascript">
	_uacct = "UA-162157-1";
	urchinTracker();
	</script>
</body>
</html>