@extends('map.layout_map')
 
@section('content')
	<div class="row">
		<div class="col-xs-3 text-center">
			<span id="drivers" class="hide" data-driver-number="<?php echo $driver_number ?>"></span>
			<span id="customers" class="hide" data-customer-number="<?php echo $customer_number ?>"></span>
			<div class="row">
				<div class="col-xs-12 text-center">
					<h4>Información de rutas</h4>
					<table class="table table-hover">
						<tr>
							<th>Conductor/Cliente</th>
							<th>Distancia</th>
							<th>Tiempo</th>
							<th>Reanudar</th>
							<th>Pausar</th>
						</tr>
						<?php for ($i=0; $i <= round(($driver_number + $customer_number) / 2); $i++): ?>
							<tr data-id="<?php echo $i; ?>" class="hide">
								<td class="names col-md-4"></td>
								<td class="distance col-md-2"></td>
								<td class="time col-md-2"></td>
								<td class="col-md-2"> 
									<button class="btn btn-success start_singular"><i class="glyphicon glyphicon-play"></i></button> 
								</td>
								<td class="col-md-2"> 
									<button class="btn btn-danger pause_singular"><i class="glyphicon glyphicon-pause"></i></button> 
								</td>
							</tr>
						<?php endfor; ?>
					</table>
					<td> 
						<button class="btn btn-success start_all">Reanudar Todos</button>
					</td>
					<td> 
						<button class="btn btn-danger pause_all">Pausar Todos</button>
					</td>
				</div>
			</div>
		</div>
		<div class="col-xs-9">
			<div id="map"></div>
		</div>
	</div>
@endsection