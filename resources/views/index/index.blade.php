@extends('index.layout')
 
@section('content')
<div class="login">
<h1>Comencemos!</h1>
<form method="post" action="/map">
    <input type="text" name="driver_number" placeholder="Conductores" required="required" />
    <input type="text" name="customer_number" placeholder="Clientes" required="required" />
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-primary btn-block btn-large" id="start">Da click aqui</button>
</form>
</div>
@endsection