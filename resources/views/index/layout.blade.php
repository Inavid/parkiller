<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prueba Parkiller</title>
 	<!-- Styles -->
	{!! Html::style('assets/bootstrap/css/bootstrap.css') !!}
	{!! Html::style('assets/css/normalize.css') !!}
	{!! Html::style('assets/css/style.css') !!}
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
	@yield('content')
 
	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	{!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
	{!! Html::script('assets/js/prefixfree.min.js') !!}
	{!! Html::script('assets/js/index.js') !!}
</body>
</html>