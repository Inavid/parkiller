var map;
var driver_number;
var customer_number;
var directionsService;
var polyline = [];
var poly2 = [];
var poly = null;
var startLocation = [];
var endLocation = [];
var timerHandle = [];
var currentDistance = [];
var speed = 0.000005, wait = 1;
var infowindow = null;
var myPano;   
var panoClient;
var nextPanoId;
var markers = [];
var lastVertex = 1;
var stepnum = 0;
var step = 50; //Avance por ciclo (5 metros)
var tick = 900; //Tiempo de cada ciclo (milisegundos)
var eol= [];

$( document ).ready(function() {
    //Se obtienen y pintan los marcadores asi como las rutas que los unen
    $.ajax({
        url: 'http://parkiller.bambut.mx/api/getInfo',
        //url: '/api/getInfo',
        data: {
            driver_number: $("#drivers").attr('data-driver-number'),
            customer_number: $("#customers").attr('data-customer-number')
        },
        dataType: 'json',
        method: 'GET',
        success: function (response) {
            var directionsDisplay = new Array();
            $.each(response, function(i, item) {
                //Se pintan markers de conductores
                if (item.driver !== null) {
                    var pointDriver = new google.maps.LatLng(item.driver.lat, item.driver.lon);
                    var imageDriver = {
                        url: item.driver.icon,
                        scaledSize: new google.maps.Size(20, 20),
                        origin: new google.maps.Point(0,0),
                        anchor: new google.maps.Point(0, 0)
                    };
                    var driver_marker = createMarker(map, pointDriver, "marker_driver " + i, imageDriver);
                };
                

                //Se pintan markers de clientes
                var pointCustomer = new google.maps.LatLng(item.customer.lat, item.customer.lon);
                var imageCustomer = {
                    url: item.customer.icon,
                    scaledSize: new google.maps.Size(20, 20),
                    origin: new google.maps.Point(0,0),
                    anchor: new google.maps.Point(0, 0)
                };
                createMarker(map, pointCustomer, "marker_customer " + i, imageCustomer);

                //Unimos clientes con conductores cercanos
                if (item.driver !== null) {
                    var request = {
                        origin: pointDriver,
                        destination: pointCustomer,
                        travelMode: google.maps.TravelMode.DRIVING
                    };

                    //Se prepara tabla con datos de conductores así como la distancia entre ellos y el tiempo estimado
                    $("tr[data-id='"+i+"']").removeClass('hide');
                    $("tr[data-id='"+i+"']").find(".names").text(item.driver.nombre+'-'+item.customer.nombre);
                    $("tr[data-id='"+i+"']").find(".distance").text(item.result_api.distance.text);
                    $("tr[data-id='"+i+"']").find(".time").text(item.result_api.duration.text);

                    //Se hace peticion a servicio de google para marcar rutas 
                    directionsService.route(request,makeRouteCallback(i,directionsDisplay[i], driver_marker, pointCustomer));
                }
            });
        }
    });

    //Evento de pausar movimiento de un conductor
    $(document).on('click', '.pause_singular', function(e){
        e.preventDefault();
        var index = $(this).parent().parent().attr('data-id');
        clearTimeout(timerHandle[index]);
    });

    //Evento de reanudar movimiento de un conductor
    $(document).on('click', '.start_singular', function(e){
        e.preventDefault();
        var index = $(this).parent().parent().attr('data-id');
        d=currentDistance[index];
        timerHandle[index] = setTimeout("animate("+index+","+d+")", tick);
    }); 

    //Evento de pausar movimiento de todos los conductores
    $(document).on('click', '.pause_all', function(e){
        e.preventDefault();
        $.each(timerHandle, function( index, value ) {
            clearTimeout(timerHandle[index]);
        });
    });  

    //Evento de reanudar movimiento de todos los conductores
    $(document).on('click', '.start_all', function(e){
        e.preventDefault();
        $.each(timerHandle, function( index, value ) {
            d=currentDistance[index];
            timerHandle[index] = setTimeout("animate("+index+","+d+")", tick);
        });        
    });  

});

//Se instancia mapa en callback de llamada a api de google
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
	    center: {lat: 19.4276298, lng: -99.128676},
	    zoom: 13
	});

    directionsService = new google.maps.DirectionsService();
}

//Función de para crear marcadores a partir del punto y el icono a utilizar
function createMarker(map, point, content, image) {  
    var marker = new google.maps.Marker({
        position: point,
        map: map,
        draggable: true,
        icon: image
    });
    google.maps.event.addListener(marker, "click", function(evt) {
        infowindow.setContent(content + "<br>" + marker.getPosition().toUrlValue(6));
        infowindow.open(map, marker);
    });
    return marker;
}

//Función para el rendereo de las rutas
function makeRouteCallback(routeNum, disp, driver_marker, pointCustomer){
    if (polyline[routeNum] && (polyline[routeNum].getMap() != null)) {
        startAnimation(routeNum);
        return;
    }
    return function(response, status){

    if (status == google.maps.DirectionsStatus.OK){

        var bounds = new google.maps.LatLngBounds();
        var route = response.routes[0];
        startLocation[routeNum] = new Object();
        endLocation[routeNum] = new Object();


        polyline[routeNum] = new google.maps.Polyline({
        path: [],
        strokeColor: '#FFFF00',
        strokeWeight: 3
        });

        poly2[routeNum] = new google.maps.Polyline({
        path: [],
        strokeColor: '#FFFF00',
        strokeWeight: 3
        });     

        var path = response.routes[0].overview_path;
        var legs = response.routes[0].legs;
        var rendererOptions = {
            map: map,
            suppressMarkers : true,
            preserveViewport: true
        };
        disp = new google.maps.DirectionsRenderer(rendererOptions);     
        disp.setMap(map);
        disp.setDirections(response);
               
        for (i=0;i<legs.length;i++) {
            if (i == 0) { 
                startLocation[routeNum].latlng = legs[i].start_location;
                startLocation[routeNum].address = legs[i].start_address;
                markers[routeNum] = driver_marker
            }
            endLocation[routeNum].latlng = legs[i].end_location;
            endLocation[routeNum].address = legs[i].end_address;
            
            var steps = legs[i].steps;

            for (j=0;j<steps.length;j++) {
                var nextSegment = steps[j].path;                
                var nextSegment = steps[j].path;

                for (k=0;k<nextSegment.length;k++) {
                    polyline[routeNum].getPath().push(nextSegment[k]);
                }
            }
        }

     }       

     polyline[routeNum].setMap(map);
     startAnimation(routeNum);  

    }
}

// Funciones de animación obtenidas de http://www.geocodezip.com/v3_SO_multipleMarkerAnimation.html               
//---------------------------------------------------------------------- 
 function updatePoly(i,d) {
 // Spawn a new polyline every 20 vertices, because updating a 100-vertex poly is too slow
    if (poly2[i].getPath().getLength() > 20) {
          poly2[i]=new google.maps.Polyline([polyline[i].getPath().getAt(lastVertex-1)]);
        }

    if (polyline[i].GetIndexAtDistance(d) < lastVertex+2) {
        if (poly2[i].getPath().getLength()>1) {
            poly2[i].getPath().removeAt(poly2[i].getPath().getLength()-1)
        }
            poly2[i].getPath().insertAt(poly2[i].getPath().getLength(),polyline[i].GetPointAtDistance(d));
    } else {
        poly2[i].getPath().insertAt(poly2[i].getPath().getLength(),endLocation[i].latlng);
    }
 }
//----------------------------------------------------------------------------

function animate(index,d) {
   if (d>eol[index]) {

      markers[index].setPosition(endLocation[index].latlng);
      return;
   }
    var p = polyline[index].GetPointAtDistance(d);

    markers[index].setPosition(p);
    updatePoly(index,d);
    timerHandle[index] = setTimeout("animate("+index+","+(d+step)+")", tick);
    currentDistance[index]=d+step;
}


//-------------------------------------------------------------------------

function startAnimation(index) {
    if (timerHandle[index]) clearTimeout(timerHandle[index]);
    eol[index]=polyline[index].Distance();
    map.setCenter(polyline[index].getPath().getAt(0));

    poly2[index] = new google.maps.Polyline({path: [polyline[index].getPath().getAt(0)], strokeColor:"#FFFF00", strokeWeight:3});

    timerHandle[index] = setTimeout("animate("+index+",50)",2000);
    currentDistance[index]=50;
}

//----------------------------------------------------------------------------    