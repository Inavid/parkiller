# Prueba de Ingreso Parkiller

La prueba consiste en pintar de manera aleatoria dentro de un area predefinida, un numero de clientes y conductores dados por el usuario. Dichos conductores deben moverse hacia su destino (Clientes), permitiendo pausar la animación (movimiento) de los mismos en cualquier momento.

La prueba fue desarrollada con el framework Laravel, utilizando la API de google maps y se coloco en una micro instancia de AWS Amazon (EC2), en caso de requerir acceso al servidor o la consola de amazon para comprobarlo favor de solicitarlo.

Se realizo una API REST que de momento solo cuenta con el método getInfo, la ruta del endpoint es:

http://parkiller.bambut.mx/api/getInfo (Se explica a detalle en wiki)

Este mismo endpoint es el que alimenta el mapa que se muestra en la pagina instalada en la instancia de amazon:

http://parkiller.bambut.mx/

En caso de requerir instalarse en otro entorno revisar o hacer lo siguiente

1.- Instalar composer y laravel
2.- Descargar código git y dar comando composer update para instalar carpeta vendor
3.- Renombrar y configurar archivo .env.example a .env, colocar la llave de api de maps en caso de requerir modificarla.
4.- Correr comando php artisan key:generate para generar llave de encriptacion

# Laravel PHP Framework
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).